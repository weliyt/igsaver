package com.weliyt.igdl.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "access_log")
public class AccessLogEntry {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    @Column(name = "datetime")
    private LocalDateTime datetime;

    @Column(name = "origin")
    private String origin;

    @Column(name = "requested_url")
    private String requestedUrl;

    public String getSimplifiedString() {
        return "Origin: " + origin + "\t Requested: " + requestedUrl;
    }

}
