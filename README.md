igdl
===
**Simple Instagram Downloader**

Mini-tool for viewing, collecting, and downloading Instagram posts  
Simple front end that submits a POST request to parse for the Instagram asset URI

A demo is deployed to https://weliyt.com/igdl

Fetched resources URLS will be saved in the browser's local storage. All resources will be loaded in the next visit.

There is an option to log requests to a database, by default this is saved to a local H2 database with tables generated from JPA.
Logging can be disabled with Spring property `access.log.db.enabled`

Default port 28015  
Standard Spring Boot execution command
```
java -jar igdl.jar 
```
  

#### API Calls
```
POST /dofetch

Body: 
[Instagram Post URL]

Response:
{
    "imgUrls": [
        "Instagram CDN URL 1",
        "Instagram CDN URL 2",
        ...
        "Instgram CDN URL n"
    ],
    "vidUrls": [
        "Instagram CDN URL 1",
        "Instagram CDN URL 2",
        ...
        "Instagram CDN URL n"
    ]
}       


Example:

Request URL: http://[host]/igdl/dofetch
Request Method: POST
Request Payload: https://www.instagram.com/p/B8Yt-4KHBLF/

Response Body:
{
   "imgUrls":[
      "https://scontent-sea1-1.cdninstagram.com/v/t51.2885-15/e35/s1080x1080/83502024_179822690003284_8526337524225062602_n.jpg?_nc_ht=scontent-sea1-1.cdninstagram.com&_nc_cat=104&_nc_ohc=PAHnW49cNTQAX9HWVAs&oh=d57f09cb5e472e69b3286180d7c030a0&oe=5EC6A864",
      "https://scontent-sea1-1.cdninstagram.com/v/t51.2885-15/e35/s1080x1080/83760160_189563578792907_6121995903474807330_n.jpg?_nc_ht=scontent-sea1-1.cdninstagram.com&_nc_cat=111&_nc_ohc=ng8IskHDBVoAX8rp-Bh&oh=25ea4fb190bf1198411cfaf530eda126&oe=5ECD4281"
   ],
   "vidUrls":[

   ]
}
```